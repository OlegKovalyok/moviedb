//
//  AppConstants.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation
import UIKit

enum Constants {
    
    enum API {
        static let scheme = "https"
        static let host = "api.themoviedb.org"
        static let version = "/3"
        static let apiKey_v3 = "c9b8a4201c607a6e9968917fd020d945"
        
        static let popularMoviesFeed = "/movie/popular"
        static let pictureAddress = "https://image.tmdb.org/t/p/w500"
        
        static let movieDetailsURL = "/movie/"
    }
    
    enum Cells {
        static let movieFeedCell = "MovieTableViewCell"
        static let cardViewCornerRadius: CGFloat = 10
    }
    
    enum Images {
        static let heart = "heart"
        static let heartFill = "heart.fill"
    }
}
