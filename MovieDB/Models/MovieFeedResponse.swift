//
//  MovieFeedResponse.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

struct MovieFeedResponse: Decodable {
    var results: [MovieFeedItem]?
    var page: Int?
}

class MovieFeedItem: Decodable {
    let id: Int
    let title: String
    let overview: String
    let releaseDate: String
    let posterPath: String?
    let voteAverage: Float
    var isFavorite: Bool? = false
    
    init(id: Int, title: String, overview: String, releaseDate: String, posterPath: String?, voteAverage: Float = 0, isFavorite: Bool) {
        self.id = id
        self.title = title
        self.overview = overview
        self.releaseDate = releaseDate
        self.voteAverage = voteAverage
        self.posterPath = posterPath
        self.isFavorite = isFavorite
    }
}

class MovieDetailsItem: MovieFeedItem {
    let revenue: Int
    
    private enum CodingKeys: String, CodingKey {
        case revenue
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        revenue = try container.decodeIfPresent(Int.self, forKey: .revenue) ?? 0
        try super.init(from: decoder)
    }
}
