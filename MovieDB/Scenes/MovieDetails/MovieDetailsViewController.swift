//
//  MovieDetailsViewController.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 23.01.20, Th.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController, MovieDetailsViewProtocol {

    @IBOutlet weak var posterImageView: WebImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var voteAverage: UILabel!
    @IBOutlet weak var budgetLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var favoritesButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    var presenter: MovieDetailsPresenterProtocol!
    var movie: MovieDetailsItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let configurator = MovieDetailsConfigurator()
        configurator.configure(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Private Methods
    
    private func configUI() {
        titleLabel.text = movie?.title
        releaseDateLabel.text = movie?.releaseDate
        overviewLabel.text = movie?.overview
        
        if let revenue = movie?.revenue {
            let budget = convertIntToCurrency(amount: revenue)
            budgetLabel.text = "Budget $" + budget
        }
        
        if let userScore = movie?.voteAverage {
            voteAverage.text = "User Score " + "\(userScore)"
        }

        if let posterPath = movie?.posterPath {
            posterImageView.set(imageURL: Constants.API.pictureAddress + posterPath)
        } else {
            posterImageView.image = #imageLiteral(resourceName: "poster-placeholder")
        }
        
        closeButton.layer.cornerRadius = closeButton.frame.width / 2
        if let isFavorite = movie?.isFavorite {
            let imageName = isFavorite ? Constants.Images.heartFill : Constants.Images.heart
            let heartImage = UIImage(systemName: imageName, compatibleWith: nil)
            favoritesButton.setImage(heartImage, for: .normal)
        }
    }
    
    private func setFavoritesImage(_ imageName: String) {
        let heartImage = UIImage(systemName: imageName, compatibleWith: nil)
        favoritesButton.setImage(heartImage, for: .normal)
    }
    
    private func convertIntToCurrency(amount: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: NSNumber(value: amount))!
    }
    
    // MARK: - User Interactions
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favoritesButtonPressed(_ sender: UIButton) {
        guard let movie = movie else { return }
        let imageName = presenter.handleFavoriteMovie(movie)
        setFavoritesImage(imageName)
    }
}
