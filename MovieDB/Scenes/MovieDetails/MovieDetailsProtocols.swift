//
//  MovieDetailsProtocols.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 23.01.20, Th.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

protocol MovieDetailsPresenterProtocol {
    func handleFavoriteMovie(_ movie: MovieFeedItem) -> String
}

protocol MovieDetailsViewProtocol: class { }
