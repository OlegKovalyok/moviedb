//
//  MovieDetailsPresenter.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 23.01.20, Th.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

class MovieDetailsPresenter: MovieDetailsPresenterProtocol {
    
    weak var view: MovieDetailsViewProtocol!
    
    // MARK: - MovieDetailsPresenterProtocol
    
    func handleFavoriteMovie(_ movie: MovieFeedItem) -> String {
        return StorageService.shared.proceedDataAndGetFavoriteName(movie)
    }
}
