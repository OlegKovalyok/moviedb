//
//  MovieDetailsConfigurator.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 23.01.20, Th.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

class MovieDetailsConfigurator {

    func configure(_ view: MovieDetailsViewController) {
        let presenter = MovieDetailsPresenter()
        view.presenter = presenter
        presenter.view = view
    }
}
