//
//  MovieFavoritesPresenter.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

class MovieFavoritesPresenter: MovieFavoritesPresenterProtocol {

    weak var view: MovieFavoritesViewProtocol!
    
    private var fetcher: DataFetcher = NetworkDataFetcher(networkService: NetworkService())
    private var movieItems: [MovieFeedItem] = []
    
    // MARK: - MovieFavoritesPresenterProtocol
    
    func getFavoritesMoviesList() {
        guard let favoriteMovies = StorageService.shared.getFavoriteMovies() else { return }
        movieItems = prepareMovieItems(favoriteMovies)
        view.showFavoritesMoviesList(movieItems: movieItems)
    }
    
    func getMovie(_ movieId: Int, completion: @escaping (MovieDetailsItem) -> Void) {
        fetcher.getMovieDetails(movieId) { (movieDetails) in
            guard let movieDetails = movieDetails else { return }
            completion(movieDetails)
        }
    }
    
    func handleFavoriteMovie(_ movie: MovieFeedItem) -> String {
        return StorageService.shared.proceedDataAndGetFavoriteName(movie)
    }
    
    // MARK: - Private Methods
    
    private func prepareMovieItems(_ storedMovies: [MovieFavorite]) -> [MovieFeedItem] {
        return storedMovies.map { MovieFeedItem(id: Int($0.id), title: $0.title, overview: $0.overview, releaseDate: $0.realeaseDate, posterPath: $0.posterPath, isFavorite: true) }
    }
}
