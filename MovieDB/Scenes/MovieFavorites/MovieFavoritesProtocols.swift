//
//  MovieFavoritesProtocols.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

protocol MovieFavoritesPresenterProtocol {
    func getFavoritesMoviesList()
    func getMovie(_ movieId: Int, completion: @escaping (MovieDetailsItem) -> Void)
    func handleFavoriteMovie(_ movie: MovieFeedItem) -> String
}

protocol MovieFavoritesViewProtocol: class {
    func showFavoritesMoviesList(movieItems: [MovieFeedItem])
}
