//
//  MovieFavoritesConfigurator.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

class MovieFavoritesConfigurator {

    func configure(_ view: MovieFavoritesViewController) {
        let presenter = MovieFavoritesPresenter()
        view.presenter = presenter
        presenter.view = view
    }
}
