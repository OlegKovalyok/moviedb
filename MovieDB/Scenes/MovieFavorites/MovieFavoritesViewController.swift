//
//  MovieFavoritesViewController.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import UIKit

class MovieFavoritesViewController: UIViewController, MovieFavoritesViewProtocol {

    @IBOutlet weak var tableView: UITableView!
    
    var presenter: MovieFavoritesPresenterProtocol!
    
    private let transition = TransitionAnimator()
    private var selectedCell = MovieTableViewCell()
    
    private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()
    
    var movieItems: [MovieFeedItem] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let configurator = MovieFavoritesConfigurator()
        configurator.configure(self)
        title = "Favorites".localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setTitles()
        presenter.getFavoritesMoviesList()
        transition.dismissCompletion = {
            self.selectedCell.isHidden = false
        }
    }
    
    // MARK: - MovieFavoritesViewProtocol
    
    func showFavoritesMoviesList(movieItems: [MovieFeedItem]) {
        self.movieItems = movieItems
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    // MARK: - Private Methods
    
    private func prepareTable() {
        tableView.dataSource = self
        tableView.delegate = self
        
        let topInset: CGFloat = 8
        tableView.contentInset.top = topInset
        
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        tableView.register(UINib(nibName: Constants.Cells.movieFeedCell, bundle: nil), forCellReuseIdentifier: MovieTableViewCell.reuseId)
        
        tableView.addSubview(refreshControl)
    }
    
    private func setTitles() {
        navigationItem.title = "Favorites".localized()
    }
    
    @objc private func refresh() {
        presenter.getFavoritesMoviesList()
    }
}

// MARK: - UITableViewDataSource

extension MovieFavoritesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.reuseId, for: indexPath) as! MovieTableViewCell
        cell.movie = movieItems[indexPath.row]
        cell.presenterFavorites = presenter
        cell.configure(typePresenter: .favorites)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension MovieFavoritesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedCell = tableView.cellForRow(at: indexPath) as! MovieTableViewCell
        presenter.getMovie(movieItems[indexPath.row].id) { (result) in
            result.isFavorite = true
            let movieDetailsViewController: MovieDetailsViewController = MovieDetailsViewController.loadFromStoryboard()
            movieDetailsViewController.movie = result
            movieDetailsViewController.transitioningDelegate = self
            movieDetailsViewController.modalPresentationStyle = .fullScreen
            self.present(movieDetailsViewController, animated: true, completion: nil)
        }
    }
}

// MARK: - UIViewControllerTransitioningDelegate

extension MovieFavoritesViewController: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let originFrame = selectedCell.superview?.convert(selectedCell.frame, to: nil) else {
            return transition
        }
        transition.originFrame = originFrame
        transition.presenting = true
        selectedCell.isHidden = true
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.presenting = false
        return transition
    }
}
