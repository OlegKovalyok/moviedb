//
//  MovieListConfigurator.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

class MovieListConfigurator {

    func configure(_ view: MovieListViewController) {
        let presenter = MovieListPresenter()
        view.presenter = presenter
        presenter.view = view
    }
}
