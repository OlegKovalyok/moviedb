//
//  MovieListPresenter.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

class MovieListPresenter: MovieListPresenterProtocol {

    weak var view: MovieListViewProtocol!
    private var fetcher: DataFetcher = NetworkDataFetcher(networkService: NetworkService())
    
    private var movieFeedResponse: MovieFeedResponse?
    private var nextPage: Int = 1
    private var movieItems: [MovieFeedItem] = []
    
    // MARK: - MovieListPresenterProtocol
    
    func getPopularMoviesList() {
        fetcher.getMovieFeed(nextPage: nil) { [weak self] (movieFeedResponse) in
            self?.movieFeedResponse = movieFeedResponse
            self?.presentMoviesFeed()
        }
    }
    
    func getNextPopularMovies() {
        nextPage = nextPage + 1
        fetcher.getMovieFeed(nextPage: String(nextPage)) { [weak self] (movieFeedResponse) in
            self?.movieFeedResponse = movieFeedResponse
            self?.presentMoviesFeed()
        }
    }
    
    func getMovie(_ movieId: Int, completion: @escaping (MovieDetailsItem) -> Void) {
        fetcher.getMovieDetails(movieId) { [weak self] (movieDetails) in
            guard let movieDetails = movieDetails else { return }
            movieDetails.isFavorite = self?.setFavoriteMovie(movieDetails.id)
            completion(movieDetails)
        }
    }
    
    func updateMoviesFeed() {
        setFavoriteMovies()
        view.showPopularMoviesList(movieItems: movieItems)
    }
    
    func handleFavoriteMovie(_ movie: MovieFeedItem) -> String {
        return StorageService.shared.proceedDataAndGetFavoriteName(movie)
    }
    
    // MARK: - Private Methods
    
    private func presentMoviesFeed() {
        guard let movieFeedResponse = movieFeedResponse,
            let movieResults = movieFeedResponse.results else { return }
        movieItems += movieResults
        setFavoriteMovies()
        view.showPopularMoviesList(movieItems: movieItems)
    }
    
    private func setFavoriteMovies() {
        let storedIds = getStoredIds()
        for movie in movieItems {
            movie.isFavorite = storedIds.filter { $0 == movie.id }.count > 0
        }
    }
    
    private func setFavoriteMovie(_ movieId: Int) -> Bool {
        let storedIds = getStoredIds()
        return !storedIds.filter { $0 == movieId }.isEmpty
    }
    
    private func getStoredIds() -> [Int] {
        guard let favoriteMovies = StorageService.shared.getFavoriteMovies() else { return [] }
        return favoriteMovies.map { Int($0.id) }
    }
}
