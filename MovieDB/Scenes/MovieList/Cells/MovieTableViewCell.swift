//
//  MovieTableViewCell.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import UIKit

enum TypePresenter { case feed, favorites }

class MovieTableViewCell: UITableViewCell {
    
    static let reuseId = "MovieCell"
    var movie: MovieFeedItem?
    var currentPresenter: TypePresenter = .feed
    var presenterFeed: MovieListPresenterProtocol?
    var presenterFavorites: MovieFavoritesPresenterProtocol?
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var posterImageView: WebImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewTextLabel: UILabel!
    @IBOutlet weak var favoritesButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        configUI()
    }

    override func prepareForReuse() {
        posterImageView.set(imageURL: nil)
    }
    
    func configure(typePresenter: TypePresenter) {
        titleLabel.text = movie?.title
        releaseDateLabel.text = movie?.releaseDate
        overviewTextLabel.text = movie?.overview
        
        if let posterPath = movie?.posterPath {
            posterImageView.set(imageURL: Constants.API.pictureAddress + posterPath)
        } else {
            posterImageView.image = #imageLiteral(resourceName: "poster-placeholder")
        }
        
        if let isFavorite = movie?.isFavorite {
            let imageName = isFavorite ? Constants.Images.heartFill : Constants.Images.heart
            let heartImage = UIImage(systemName: imageName, compatibleWith: nil)
            favoritesButton.setImage(heartImage, for: .normal)
        }
        currentPresenter = typePresenter
    }
    
    // MARK: - User Interactions
    
    @IBAction func favoritesButtonPressed(_ sender: UIButton) {
        guard let movie = movie else { return }
        
        switch currentPresenter {
        case .feed:
            guard let presenterFeed = presenterFeed else { return }
            let imageName = presenterFeed.handleFavoriteMovie(movie)
            setFavoritesImage(imageName)
            presenterFeed.updateMoviesFeed()
        case .favorites:
            guard let presenterFavorites = presenterFavorites else { return }
            let imageName = presenterFavorites.handleFavoriteMovie(movie)
            setFavoritesImage(imageName)
            presenterFavorites.getFavoritesMoviesList()
        }
    }
    
    // MARK: - Private Methods
    
    private func configUI() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
        
        cardView.layer.cornerRadius = Constants.Cells.cardViewCornerRadius
        cardView.clipsToBounds = true
    }
    
    private func setFavoritesImage(_ imageName: String) {
        let heartImage = UIImage(systemName: imageName, compatibleWith: nil)
        favoritesButton.setImage(heartImage, for: .normal)
    }
}
