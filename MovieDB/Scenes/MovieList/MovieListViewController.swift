//
//  MovieListViewController.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController, MovieListViewProtocol {

    @IBOutlet weak var tableView: UITableView!
    
    private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()
    private lazy var footerView = FooterView()
    private let transition = TransitionAnimator()
    private var selectedCell = MovieTableViewCell()
    
    var presenter: MovieListPresenterProtocol!
    
    var movieItems: [MovieFeedItem] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let configurator = MovieListConfigurator()
        configurator.configure(self)
        title = "Feed".localized()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareTable()
        presenter.getPopularMoviesList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setTitles()
        presenter.updateMoviesFeed()
        transition.dismissCompletion = {
            self.selectedCell.isHidden = false
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if (bottomEdge >= scrollView.contentSize.height) {
            presenter.getNextPopularMovies()
            footerView.showLoader()
        }
    }
    
    // MARK: - MovieListViewProtocol
    
    func showPopularMoviesList(movieItems: [MovieFeedItem]) {
        self.movieItems = movieItems
        tableView.reloadData()
        refreshControl.endRefreshing()
        if footerView.isAnimated {
            footerView.dismissLoader()
        }
    }
    
    // MARK: - Private Methods
    
    private func prepareTable() {
        tableView.dataSource = self
        tableView.delegate = self
        
        let topInset: CGFloat = 8
        tableView.contentInset.top = topInset
        
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        tableView.register(UINib(nibName: Constants.Cells.movieFeedCell, bundle: nil), forCellReuseIdentifier: MovieTableViewCell.reuseId)
        
        tableView.addSubview(refreshControl)
        tableView.tableFooterView = footerView
    }
    
    private func setTitles() {
        navigationItem.title = "Popular".localized()
    }
    
    @objc private func refresh() {
        presenter.getPopularMoviesList()
    }

}

// MARK: - UITableViewDataSource

extension MovieListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.reuseId, for: indexPath) as! MovieTableViewCell
        cell.movie = movieItems[indexPath.row]
        cell.presenterFeed = presenter
        cell.configure(typePresenter: .feed)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension MovieListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedCell = tableView.cellForRow(at: indexPath) as! MovieTableViewCell
        presenter.getMovie(movieItems[indexPath.row].id) { (result) in
            let movieDetailsViewController: MovieDetailsViewController = MovieDetailsViewController.loadFromStoryboard()
            movieDetailsViewController.movie = result
            movieDetailsViewController.transitioningDelegate = self
            movieDetailsViewController.modalPresentationStyle = .fullScreen
            self.present(movieDetailsViewController, animated: true, completion: nil)
        }
    }
}

// MARK: - UIViewControllerTransitioningDelegate

extension MovieListViewController: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let originFrame = selectedCell.superview?.convert(selectedCell.frame, to: nil) else {
            return transition
        }
        transition.originFrame = originFrame
        transition.presenting = true
        selectedCell.isHidden = true
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.presenting = false
        return transition
    }
}
