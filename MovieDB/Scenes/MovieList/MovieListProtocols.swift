//
//  MovieListProtocols.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

protocol MovieListPresenterProtocol: class {
    func getPopularMoviesList()
    func getNextPopularMovies()
    func updateMoviesFeed()
    func getMovie(_ movieId: Int, completion: @escaping (MovieDetailsItem) -> Void)
    func handleFavoriteMovie(_ movie: MovieFeedItem) -> String
}

protocol MovieListViewProtocol: class {
    func showPopularMoviesList(movieItems: [MovieFeedItem])
}
