//
//  FooterView.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 15.02.20, Sa.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation
import UIKit

class FooterView: UIView {
    
    var isAnimated: Bool
    
    private var loader: UIActivityIndicatorView = {
       let loader = UIActivityIndicatorView()
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.color = .white
        loader.hidesWhenStopped = true
        return loader
    }()
    
    override init(frame: CGRect) {
        isAnimated = true
        super.init(frame: frame)
        
        addSubview(loader)
        
        loader.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        loader.topAnchor.constraint(equalTo: bottomAnchor, constant: 8).isActive = true
    }
    
    func showLoader() {
        loader.startAnimating()
        isAnimated = true
    }
    
    func dismissLoader() {
        loader.stopAnimating()
        isAnimated = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
