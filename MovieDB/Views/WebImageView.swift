//
//  WebImageView.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 23.01.20, Th.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import UIKit

class WebImageView: UIImageView {
    
    private var currentUrlString: String?
    
    func set(imageURL: String?) {
        guard let imageURL = imageURL, let url = URL(string: imageURL) else {
            self.image = nil
            return
        }
        
        currentUrlString = imageURL
        
        // MARK: Fetch cached data
        
        DispatchQueue.main.async {
            if let cachedResponse = URLCache.shared.cachedResponse(for: URLRequest(url: url)) {
                self.image = UIImage(data: cachedResponse.data)
                return
            }
        }
        
        // MARK: Fetch data from internet

        let dataTask = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            let queue = DispatchQueue.global(qos: .utility)
            queue.async {
                if let data = data, let response = response {
                    self?.handleLoadedImage(data: data, response: response)
                }
            }
        }
        dataTask.resume()
    }
    
    // MARK: - Private Methods
    
    private func handleLoadedImage(data: Data, response: URLResponse) {
        guard let responseURL = response.url else { return }
        let cachedResponse = CachedURLResponse(response: response, data: data)
        URLCache.shared.storeCachedResponse(cachedResponse, for: URLRequest(url: responseURL))
        
        if responseURL.absoluteString == currentUrlString {
            DispatchQueue.main.async {
                self.image = UIImage(data: data)
            }
        }
    }
}
