//
//  StringExtension.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 18.02.20, Tu.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

extension String {
    public func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
