//
//  NetworkDataFetcher.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

protocol DataFetcher {
    
    func getMovieFeed(nextPage: String?, response: @escaping (MovieFeedResponse?) -> Void)
    func getMovieDetails(_ movieId: Int, response: @escaping (MovieDetailsItem?) -> Void)
}

struct NetworkDataFetcher: DataFetcher {

    let networkService: Networking
    
    init(networkService: Networking) {
        self.networkService = networkService
    }
    
    func getMovieFeed(nextPage: String?, response: @escaping (MovieFeedResponse?) -> Void) {
        
        var params = ["api_key": Constants.API.apiKey_v3]
        params["page"] = nextPage ?? "1"
        
        networkService.request(path: Constants.API.popularMoviesFeed, params: params) { (data, error) in
            if let error = error {
                print("\(error.localizedDescription)")
                response(nil)
            }
            
            let decoded = self.decodeJSON(type: MovieFeedResponse.self, from: data)
            response(decoded)
        }
    }
    
    func getMovieDetails(_ movieId: Int, response: @escaping (MovieDetailsItem?) -> Void) {
        
        let movie = String(movieId)
        let params = ["api_key": Constants.API.apiKey_v3]
        
        networkService.request(path: Constants.API.movieDetailsURL + movie, params: params) { (data, error) in
            if let error = error {
                print("\(error.localizedDescription)")
                response(nil)
            }
            
            let decoded = self.decodeJSON(type: MovieDetailsItem.self, from: data)
            response(decoded)
        }
    }
    
    // MARK: - Private Methods
    
    private func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let data = from, let response = try? decoder.decode(type.self, from: data) else { return nil }
        return response
    }
}
