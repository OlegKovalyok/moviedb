//
//  StorageService.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 24.01.20, Fr.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation
import CoreData

final class StorageService {

    static let shared: StorageService = {
        let instance = StorageService()
        return instance
    }()
    private init() { }
    
    // MARK: - Setup
    
    let persistentContainer = NSPersistentContainer(name: "MovieDB")
    var context: NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    func initalizeStack(completion: @escaping () -> Void) {
        self.persistentContainer.loadPersistentStores { description, error in
            if let error = error {
                print("could not load store \(error.localizedDescription)")
                return
            }
            print("store loaded")
            completion()
        }
    }
    
    func setStore(type: String) {
        let description = NSPersistentStoreDescription()
        description.type = type // types: NSInMemoryStoreType, NSSQLiteStoreType, NSBinaryStoreType
        
        if type == NSSQLiteStoreType || type == NSBinaryStoreType {
            description.url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
                .first?.appendingPathComponent("database")
        }
        
        self.persistentContainer.persistentStoreDescriptions = [description]
    }
    
    // MARK: - Public CoreData's Interface
    
    func findFavoriteMovie(_ id: Int) -> MovieFavorite? {
        let movie = try? fetchMovie(withId: id).first
        return movie
    }
    
    func getFavoriteMovies() -> [MovieFavorite]? {
        let movies = try? self.fetchMovies()
        return movies
    }
    
    func addFavoriteMovie(_ movie: MovieFeedItem) {
        create(with: movie.id, title: movie.title, releaseDate: movie.releaseDate, overview: movie.overview, posterPath: movie.posterPath)
    }
    
    func deleteFavoriteMovie(_ movie: MovieFavorite) {
        try? delete(movie: movie)
    }
    
    // MARK: - Public Methods
    
    func proceedDataAndGetFavoriteName(_ movie: MovieFeedItem) -> String {
        if let movie = findFavoriteMovie(movie.id) {
            deleteFavoriteMovie(movie)
            return Constants.Images.heart
        } else {
            addFavoriteMovie(movie)
            return Constants.Images.heartFill
        }
    }
    
    // MARK: - Private CoreData's Methods
    
    private func create(with id: Int, title: String, releaseDate: String, overview: String, posterPath: String?) {
        let movieFavorite = MovieFavorite(context: context)
        movieFavorite.id = Int64(id)
        movieFavorite.title = title
        movieFavorite.realeaseDate = releaseDate
        movieFavorite.overview = overview
        movieFavorite.posterPath = posterPath
        
        try? self.insert(movie: movieFavorite)
    }
    
    private func fetchMovies() throws -> [MovieFavorite] {
        let movies = try self.context.fetch(MovieFavorite.fetchRequest() as NSFetchRequest<MovieFavorite>)
        return movies
    }

    private func fetchMovie(withId id: Int) throws -> [MovieFavorite] {
        let request = NSFetchRequest<MovieFavorite>(entityName: "MovieFavorite")
        request.predicate = NSPredicate(format: "id == %d", Int64(id))

        let movies = try self.context.fetch(request)
        return movies
    }
    
    private func insert(movie: MovieFavorite) throws {
        self.context.insert(movie)
        try self.context.save()
    }
    
    private func delete(movie: MovieFavorite) throws {
        self.context.delete(movie)
        try self.context.save()
    }
}

extension StorageService: NSCopying {

    func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
}
