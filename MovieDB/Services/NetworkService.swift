//
//  NetworkService.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 22.01.20, We.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//

import Foundation

protocol Networking {
    
    func request(path: String, params: [String: String], completion: @escaping (Data?, Error?) -> Void)
}

final class NetworkService: Networking {

    // MARK: - Networking
    
    func request(path: String, params: [String: String], completion: @escaping (Data?, Error?) -> Void) {
        
        if let url = getUrl(from: path, params: params) {
            print(url)
            let request = URLRequest(url: url)
            let task = createDataTask(from: request, completion: completion)
            task.resume()
        } else {
            print("Invalid URL")
        }
    }
    
    // MARK: - Private Methods
    
    private func getUrl(from path: String, params: [String: String]) -> URL? {
        var components = URLComponents()
        components.scheme = Constants.API.scheme
        components.host = Constants.API.host
        components.path = Constants.API.version + path
        components.queryItems = params.map { URLQueryItem(name: $0, value: $1) }
        
        return components.url
    }
    
    private func createDataTask(from request: URLRequest,
                                completion: @escaping (Data?, Error?) -> Void) -> URLSessionDataTask {
        return URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                completion(data, error)
            }
        })
    }
}
