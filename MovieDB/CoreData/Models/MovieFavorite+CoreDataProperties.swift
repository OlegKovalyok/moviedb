//
//  MovieFavorite+CoreDataProperties.swift
//  MovieDB
//
//  Created by Oleg Kovalyok on 24.01.20, Fr.
//  Copyright © 2020 Oleg Kovalyok. All rights reserved.
//
//

import Foundation
import CoreData

extension MovieFavorite {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MovieFavorite> {
        return NSFetchRequest<MovieFavorite>(entityName: "MovieFavorite")
    }

    @NSManaged public var id: Int64
    @NSManaged public var title: String
    @NSManaged public var realeaseDate: String
    @NSManaged public var overview: String
    @NSManaged public var posterPath: String?

}
